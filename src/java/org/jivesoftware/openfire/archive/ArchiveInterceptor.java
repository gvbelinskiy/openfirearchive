/*
 * Copyright (C) 2008 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jivesoftware.openfire.archive;

import org.jivesoftware.openfire.cluster.ClusterManager;
import org.jivesoftware.openfire.interceptor.InterceptorManager;
import org.jivesoftware.openfire.interceptor.PacketInterceptor;
import org.jivesoftware.openfire.interceptor.PacketRejectedException;
import org.jivesoftware.openfire.session.Session;
import org.jivesoftware.util.XMPPDateTimeFormat;
import org.picocontainer.Startable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.JID;
import org.xmpp.packet.Message;
import org.xmpp.packet.Packet;

import com.reucon.openfire.plugin.archive.model.ArchivedMessage;
//import com.reucon.openfire.plugin.archive.model.MemoryDelivered;
//import com.reucon.openfire.plugin.archive.model.MemoryList;
//import com.reucon.openfire.plugin.archive.model.MemorySet;
//import com.reucon.openfire.plugin.archive.model.MemoryViewed;
import com.reucon.openfire.plugin.archive.model.ArchivedMessage.Direction;
import com.reucon.openfire.plugin.archive.model.ChatStore;

import org.dom4j.Element;
import java.util.Date;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import java.util.TimeZone;

import org.jivesoftware.openfire.SessionManager;
import org.jivesoftware.openfire.auth.FastMemory;


/**
 * Intercepts packets to track conversations. Only the following messages
 * are processed:
 * <ul>
 *  <li>Messages sent between local users.</li>
 *  <li>Messages sent between local user and remote entities (e.g. remote users).</li>
 *  <li>Messages sent between local users and users using legacy networks (i.e. transports).</li>
 * </ul>
 * Therefore, messages that are sent to Publish-Subscribe or any other internal service are ignored.
 *
 * @author Matt Tucker
 */
public class ArchiveInterceptor implements PacketInterceptor, Startable {

    private ConversationManager conversationManager;
    private static final Logger Log = LoggerFactory.getLogger(ArchiveInterceptor.class);
    public ArchiveInterceptor(ConversationManager conversationManager) {
        this.conversationManager = conversationManager;
    }
    private String makeChatId(Packet packet) {
		String rezult=null;
    	try{
    	String pfrom=getUserName(packet.getFrom().toBareJID());
    	String pto=getUserName(packet.getTo().toBareJID());
    	String subkeya="";
    	String subkeyb="";
    	
    	if(pfrom.compareTo(pto)>0) {
    		// 1 - pfrom больше чем pto
    		subkeya=pfrom;
    		subkeyb=pto;
    	}else {
    		subkeya=pto;
    		subkeyb=pfrom;
    	}
    	
    	rezult=subkeya+subkeyb;
    	}catch(Exception e) {
    	 Log.debug( "IQRetrieveHandler: makeChatId error.", e.toString() );
    	}
    	return rezult;
	}
    private String getUserName(String username) {
		if(username==null) return "";
		if(username.contains("@")) {
	         	 int index = username.indexOf("@");
	             username = username.substring(0, index);
	     }
		return username;
		
	}

    public void interceptPacket(Packet packet, Session session, boolean incoming, boolean processed)
            throws PacketRejectedException
    {
        // Ignore any packets that haven't already been processed by interceptors.
        if (!processed) {
            return;
        }
        
        try{
        	
        	if (packet.getFrom() != null)
        	{
        		String fruser=packet.getFrom().toBareJID();
        	// cont msg to stop flood
        		/*
        	try{
        		MemoryMap mm=new MemoryMap(); 
        		
        		Map<String, Integer> userMap=mm.getLockUser();
        		if(userMap.containsKey(fruser)) {
        			int totalpackets=userMap.get(fruser);
        			if(totalpackets>1000) {
        				  Log.debug("setBanToUser: 1000 packets in 5 min="+fruser);
        				  
        				FastMemory ff=new FastMemory();
        				ff.setBanToUser(fruser);
        				
        				
        				try {
        					String plit =fruser; 
        					 if(fruser.contains("@")) {
        					 String[] arrSplit_2 = fruser.split("@");
        					  plit = arrSplit_2[0];}
        					for (Session ses : SessionManager.getInstance().getSessions(plit)) {
        					 try {ses.close();}catch (Exception i) {}
        					}
        					}catch (Exception c) {}
        				
        			}else {
        				int add=totalpackets+1;
        				userMap.put(fruser, add);
        			}
        				
        			
        		}else {
        			userMap.put(fruser, 1);
        		}
        		
        	
        	}catch(Exception e){ Log.debug("MemoryMap: exception="+e.toString());}
        	*/
        }}catch(Exception e){}
        
        if (packet instanceof Message) {
            // Ignore any outgoing messages (we'll catch them when they're incoming).
            if (!incoming) {
                return;
            }
            Message message = (Message) packet;
            String stanza = null;
            try{stanza=message.getID();}catch(Exception e){}
            
            
            
            // Ignore any messages that don't have a body so that we skip events.
            // Note: XHTML messages should always include a body so we should be ok. It's
            // possible that we may need special XHTML filtering in the future, however.
            if (message.getBody() != null) {
                // Only process messages that are between two users, group chat rooms, or gateways.
            	
            	
            //find request element
            	 String modrequestID=null;
            	 String delrequestID=null;
            	 String text=null;
            	 String symKeyUUID=null;
            	 String mtype=null;
            	 String chatid=null;
            	 boolean todelete=false;
            	 boolean deleteall=false;
            	 boolean edit=false;
            	 String tdata="tdata=";
            	 
            	
            	 
               	 try{
               		Element correct =null;
               				Element delete  =null;
               		 try{ correct = message.getChildElement("request", "urn:xmpp:message-correct"); }catch (Exception e) {}
               		 try{ delete  = message.getChildElement("request", "urn:xmpp:message-delete"); }catch (Exception e) {}
                    	if(correct!=null){
                    		modrequestID=correct.attributeValue("id");
                    		text=correct.elementText("body");
                    		edit=true;
                    		
                    		tdata=tdata+"modrequestID="+modrequestID;
                    		tdata=tdata+"text="+text;
                    	}
                    	else if(delete!=null){
                    		delrequestID=delete.attributeValue("id");
                    		if(delrequestID.equals("deleteall"))
                    			deleteall=true;
                    			else
                    				todelete=true;
                    		
                    		tdata=tdata+" delrequestID="+delrequestID;
                    		
                    		
                    	}
                    }catch (Exception e) {}
            	
               	try{symKeyUUID=message.getElement().attributeValue("encrypted"); }catch (Exception e) {}
                try{ mtype=message.getElement().attributeValue("mtype"); }catch (Exception e) {}
                try{ chatid=message.getElement().attributeValue("chatid"); }catch (Exception e) {}
                if(chatid==null) {
                	chatid=makeChatId(packet);
                }
                if(chatid.length()<5) {
                	chatid=makeChatId(packet);
                }
                
                if (conversationManager.isConversation(message)) { 
                	try{
                    // Process this event in the senior cluster member or local JVM when not in a cluster
                    if (ClusterManager.isSeniorClusterMember()) {
                    	      
                    		 if(chatid!=null) {
                    			Date msgDate=new Date(); 
                    	    	ArchivedMessage archivedMessage= new ArchivedMessage(msgDate, Direction.to, null, message.getTo(), stanza, "-1", "-1", modrequestID, delrequestID, text, delrequestID, symKeyUUID, mtype, chatid);
                    	    	archivedMessage.setFromJid(message.getFrom());                                 
                    	    	archivedMessage.setBody(message.getBody());
                    	    	
                    	    	Log.debug("MemorySet: chatid="+chatid);
                    	    	ChatStore store=new ChatStore();
                    	    try {	
                    	    	String toStr=message.getTo().getNode();
                    	    	String fromStr=message.getFrom().getNode();
                    	    	addJidChaitID( msgDate,  toStr,  fromStr,  chatid,  store, archivedMessage);
                    	    	
                    		 }catch (Exception jch) {
                             	Log.error("ArchiveInterceptor error="+jch.toString());
                             }
                    	    /*
                    	    	MemoryList mlist=new MemoryList();
                    	    	String suffix = new SimpleDateFormat(XMPPDateTimeFormat.XMPP_DATE_FORMAT).format(msgDate);
                    	    	 Log.debug("MemoryList: resp start="+chatid+"_"+suffix); 
                    	    	 
                    	    	 if(mlist.getChatTime().containsKey(chatid+"_"+suffix)) {}else{
                    	    	 Map<String, String> chatTime=new ConcurrentHashMap<String, String>();
                    	    	
                    	    	 SimpleDateFormat dateFormat = new SimpleDateFormat(XMPPDateTimeFormat.XMPP_DATETIME_FORMAT);
                    	         dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    	         String beginTime=dateFormat.format(msgDate);
                    	         Log.debug("MemoryList: put="+chatid+"_"+suffix); 
                    	         chatTime.put(chatid+"_"+suffix, beginTime);
                    	         mlist.setChatTime(chatTime);
                    	         }
                    	    	
                    	    	MemorySet mset=new MemorySet();
                    	    	Map<String, ConcurrentHashMap<String, ArchivedMessage>> conversations = mset.getConversations();
                    	    	if(conversations!=null){
                    			if(conversations.containsKey(chatid)) {
                    				ConcurrentHashMap<String, ArchivedMessage> value = conversations.get(chatid);
                    				String mstanza=null;
                    				 try { mstanza= conversation.getStanza();}catch(Exception e) {}
                    				    if(mstanza!=null) {
                    					   value.put(mstanza, conversation);
                    					   conversations.put(chatid, value);
                    					   Log.debug("MemorySet: put="+conversation.toString());
                    				}
                    			}
                    			else {
                    				ConcurrentHashMap<String, ArchivedMessage> value=new ConcurrentHashMap<String, ArchivedMessage>();
                    				String mstanza=null;
                    				try { mstanza= conversation.getStanza();}catch(Exception e) {}
                    				if(mstanza!=null) {
                    					value.put(stanza, conversation);
                    					conversations.put(chatid, value);
                    					Log.debug("MemorySet: put new="+conversation.toString());
                    				}
                    				
                    			}
                    	    	//MemorySet.addConversation(chatid, conversation);
                    		  }else{
                    			  Log.debug("MemorySet: mset.getConversations is null");
                    		  }
                    	    	
                    		 */
                    		 }
                          	  
                    	      conversationManager.processMessage(message.getFrom(), message.getTo(), message.getBody(), stanza, new Date(), modrequestID, delrequestID, text, symKeyUUID, mtype, chatid);
                    	
                    }
                    else {
                        JID sender = message.getFrom();
                        JID receiver = message.getTo();
                        ConversationEventsQueue eventsQueue = conversationManager.getConversationEventsQueue();
                        eventsQueue.addChatEvent(conversationManager.getConversationKey(sender, receiver),
                                ConversationEvent.chatMessageReceived(sender, receiver,
                                        conversationManager.isMessageArchivingEnabled() ? message.getBody() : null,
                                        new Date()));
                    }
                    
                    if(edit) {
                    	tdata=tdata+" edit";
                        conversationManager.processMessageCorrect(message.getFrom(), message.getTo(), message.getBody(), stanza, new Date(), modrequestID, delrequestID, text, symKeyUUID, chatid);}
                    else if(todelete) {
                    	tdata=tdata+" delete";
                        conversationManager.processMessageDelete(message.getFrom(), message.getTo(), message.getBody(), stanza, new Date(), modrequestID, delrequestID, text, chatid);}
                    else if(deleteall)
                        conversationManager.processMessageDeleteAll(message.getFrom(), message.getTo(), message.getBody(), stanza, new Date(), modrequestID, delrequestID, text, chatid);
                    
                }catch (Exception ecx) {
                	Log.error("ArchiveInterceptor error="+ecx.toString());
                }
                }
            }else {

           	 String confirmmessageID=null; 
           	 try{
           		
                	Element dat = message.getChildElement("received", "urn:xmpp:receipts");
                	if(dat!=null){
                	    confirmmessageID=dat.attributeValue("id");
                	    //String out=" r="+confirmmessageID+" /n";
                	     //Files.write(Paths.get("/opt/openfire/logs/myfile.txt"), out.getBytes(), StandardOpenOption.APPEND);
                	}	
                }catch (Exception e) {}
           	 
           	 String viewmmessageID=null; 
           	 try{
           		
                	Element dat = message.getChildElement("viewed", "urn:xmpp:receipts");
                	if(dat!=null){
                		viewmmessageID=dat.attributeValue("id");
                		 //String outa=" v="+viewmmessageID+" /n";
                         	    //Files.write(Paths.get("/opt/openfire/logs/myfile.txt"), outa.getBytes(), StandardOpenOption.APPEND);
                	}	
                }catch (Exception e) {}
           	 
           	 
           	 if(confirmmessageID!=null) {
           		 if (ClusterManager.isSeniorClusterMember()) {
           			    ChatStore store=new ChatStore();
           			    store.getReadStorequinceM().put(confirmmessageID, "1");
           			    store.getReadStoreoneH().put(confirmmessageID, "1");
           			 	//MemoryDelivered.setDeliveryElement(confirmmessageID);
                        conversationManager.processMessageConfirm(confirmmessageID,message.getFrom(), message.getTo());
                       
           		 }
                   
           	 }
           	 else if(viewmmessageID!=null) {
           		 
           		 if (ClusterManager.isSeniorClusterMember()) {
           			     ChatStore store=new ChatStore();
           			     store.getViewStorequinceM().put(viewmmessageID, "1");
           			     store.getViewStoreoneH().put(viewmmessageID, "1");
           			 	 //MemoryViewed.setViewElement(viewmmessageID);
                        conversationManager.processMessageViewed(viewmmessageID,message.getFrom(), message.getTo());
                      
           		 }
           	 }
           	 
           	 
           	 
           	 
           	 
           	
           
            }
        }
    }
public void addJidChaitID(Date msgDate, String toStr, String fromStr, String chatid, ChatStore store, ArchivedMessage archivedMessage) {
	
	 SimpleDateFormat dateFormat = new SimpleDateFormat(XMPPDateTimeFormat.XMPP_DATETIME_FORMAT);
     dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
     String beginTime=dateFormat.format(msgDate);
     
     //--------------------------------------------jidChatIdStore------------------------------------------------------ 	
	Map<String, ConcurrentHashMap<String, String>> jidChatIdStorequinceM = store.getChatIdJidStorequinceM();
	if(toStr!=null)
	if(jidChatIdStorequinceM.containsKey(toStr)){
		Map<String, String> chatis = jidChatIdStorequinceM.get(toStr);
		if(chatis!=null) {
			if(!chatis.containsKey(chatid)) {
				ConcurrentHashMap<String, String> chatquinceM=new ConcurrentHashMap<String, String>();
				chatquinceM.put(chatid, beginTime);
				jidChatIdStorequinceM.put(toStr, chatquinceM);
			}
			
		}
	}else {
		ConcurrentHashMap<String, String> chatquinceM=new ConcurrentHashMap<String, String>();
		chatquinceM.put(chatid, beginTime);
		jidChatIdStorequinceM.put(toStr, chatquinceM);
	}
	
	
	if(fromStr!=null)
		if(jidChatIdStorequinceM.containsKey(fromStr)){
    		Map<String, String> chatis = jidChatIdStorequinceM.get(fromStr);
    		if(chatis!=null) {
    			if(!chatis.containsKey(chatid)) {
    				ConcurrentHashMap<String, String> chatquinceM=new ConcurrentHashMap<String, String>();
    				chatquinceM.put(chatid, beginTime);
    				jidChatIdStorequinceM.put(fromStr, chatquinceM);
    			}
    			
    		}
    	}else {
    		ConcurrentHashMap<String, String> chatquinceM=new ConcurrentHashMap<String, String>();
			chatquinceM.put(chatid, beginTime);
			jidChatIdStorequinceM.put(fromStr, chatquinceM);
    	}
	
	Map<String, ConcurrentHashMap<String, String>> jidChatIdStoreoneH = store.getChatIdJidStoreoneH();
	if(toStr!=null)
	if(jidChatIdStoreoneH.containsKey(toStr)){
		Map<String, String> chatis = jidChatIdStoreoneH.get(toStr);
		if(chatis!=null) {
			if(!chatis.containsKey(chatid)) {
				ConcurrentHashMap<String, String> chatquinceM=new ConcurrentHashMap<String, String>();
				chatquinceM.put(chatid, beginTime);
				jidChatIdStoreoneH.put(toStr, chatquinceM);
			}
			
		}
	}else {
		ConcurrentHashMap<String, String> chatquinceM=new ConcurrentHashMap<String, String>();
		chatquinceM.put(chatid, beginTime);
		jidChatIdStoreoneH.put(toStr, chatquinceM);
	}
	
	if(fromStr!=null)
		if(jidChatIdStoreoneH.containsKey(fromStr)){
    		Map<String, String> chatis = jidChatIdStoreoneH.get(fromStr);
    		if(chatis!=null) {
    			if(!chatis.containsKey(chatid)) {
    				ConcurrentHashMap<String, String> chatquinceM=new ConcurrentHashMap<String, String>();
    				chatquinceM.put(chatid, beginTime);
    				jidChatIdStoreoneH.put(fromStr, chatquinceM);
    			}
    			
    		}
    	}else {
    		ConcurrentHashMap<String, String> chatquinceM=new ConcurrentHashMap<String, String>();
			chatquinceM.put(chatid, beginTime);
			jidChatIdStoreoneH.put(fromStr, chatquinceM);
    	}
	     //--------------------------------------------ListStore------------------------------------------------------ 
	
	 		Map<String, Long> timeStorequinceM =store.getTimeStorequinceM();
	 		if(!timeStorequinceM.containsKey(toStr)) {
	 			timeStorequinceM.put(toStr, msgDate.getTime());
	 		 }
	 		
	 		Map<String, Long> timeStoreoneH =store.getTimeStoreoneH();
	 		if(!timeStoreoneH.containsKey(toStr)) {
	 			timeStoreoneH.put(toStr, msgDate.getTime());
	 		 }
	
		  Map<String, String> listquinceM = store.getListStorequinceM();
		 if(!listquinceM.containsKey(chatid)) {
	         listquinceM.put(chatid,beginTime);
	       }
		 
			Map<String, String> listoneH = store.getListStoreoneH();
			 if(!listoneH.containsKey(chatid)) {
				 listoneH.put(chatid,beginTime);
		       }
			//--------------------------------------------MessageCounters----------------------------------------------
			 store.uponeH(chatid); //slow read fast write
			 
			//--------------------------------------------MessageStore------------------------------------------------- 
			Map<String, ConcurrentHashMap<String, ArchivedMessage>> conversationsquinceM = store.getMessageStorequinceM();
 	    	if(conversationsquinceM!=null){
	 			if(conversationsquinceM.containsKey(chatid)) {
	 				ConcurrentHashMap<String, ArchivedMessage> value = conversationsquinceM.get(chatid);
	 				String mstanza=null;
	 				 try { mstanza= archivedMessage.getStanza();}catch(Exception e) {}
	 				    if(mstanza!=null) {
	 					   value.put(mstanza, archivedMessage);
	 					  conversationsquinceM.put(chatid, value);
	 					   Log.debug("MessageStore: put="+archivedMessage.toString());
	 				}
	 			}
	 			else {
	 				ConcurrentHashMap<String, ArchivedMessage> value=new ConcurrentHashMap<String, ArchivedMessage>();
	 				String mstanza=null;
	 				try { mstanza= archivedMessage.getStanza();}catch(Exception e) {}
	 				if(mstanza!=null) {
	 					value.put(archivedMessage.getStanza(), archivedMessage);
	 					conversationsquinceM.put(chatid, value);
	 					Log.debug("MessageStore: put new="+archivedMessage.toString());
	 				}
	 				
	 			}
 	    	 }else{Log.debug("MessageStore: mset.getConversations is null");}

			Map<String, ConcurrentHashMap<String, ArchivedMessage>> conversationsoneH = store.getMessageStoreoneH();
 	    	if(conversationsoneH!=null){
	 			if(conversationsoneH.containsKey(chatid)) {
	 				ConcurrentHashMap<String, ArchivedMessage> value = conversationsoneH.get(chatid);
	 				String mstanza=null;
	 				 try { mstanza= archivedMessage.getStanza();}catch(Exception e) {}
	 				    if(mstanza!=null) {
	 					   value.put(mstanza, archivedMessage);
	 					  conversationsoneH.put(chatid, value);
	 					   Log.debug("MessageStore: put="+archivedMessage.toString());
	 				}
	 			}
	 			else {
	 				ConcurrentHashMap<String, ArchivedMessage> value=new ConcurrentHashMap<String, ArchivedMessage>();
	 				String mstanza=null;
	 				try { mstanza= archivedMessage.getStanza();}catch(Exception e) {}
	 				if(mstanza!=null) {
	 					value.put(archivedMessage.getStanza(), archivedMessage);
	 					conversationsoneH.put(chatid, value);
	 					Log.debug("MessageStore: put new="+archivedMessage.toString());
	 				}
	 				
	 			}
 	    	 }else{Log.debug("MessageStore: getConversations is null");}
 			
}
    public void start() {
        InterceptorManager.getInstance().addInterceptor(this);
    }

    public void stop() {
        InterceptorManager.getInstance().removeInterceptor(this);
        conversationManager = null;
    }
}
