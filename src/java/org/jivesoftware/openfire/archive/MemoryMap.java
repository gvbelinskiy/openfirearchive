package org.jivesoftware.openfire.archive;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryMap {
	static Map<String, Integer> lockUser=new ConcurrentHashMap<String, Integer>();

	public static Map<String, Integer> getLockUser() {
		return lockUser;
	}

	public static void setLockUser(Map<String, Integer> lockUser) {
		MemoryMap.lockUser = lockUser;
	}
	
	
}
