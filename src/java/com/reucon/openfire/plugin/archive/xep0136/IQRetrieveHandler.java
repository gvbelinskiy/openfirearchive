package com.reucon.openfire.plugin.archive.xep0136;

import com.reucon.openfire.plugin.archive.PersistenceManager;
import com.reucon.openfire.plugin.archive.model.ArchivedMessage;
import com.reucon.openfire.plugin.archive.model.ChatStore;
import com.reucon.openfire.plugin.archive.model.Conversation;
import com.reucon.openfire.plugin.archive.model.MemoryDelivered;
import com.reucon.openfire.plugin.archive.model.MemoryPoint;
import com.reucon.openfire.plugin.archive.model.MemorySet;
import com.reucon.openfire.plugin.archive.model.MemoryViewed;
import com.reucon.openfire.plugin.archive.model.ArchivedMessage.Direction;
import com.reucon.openfire.plugin.archive.util.XmppDateUtil;
import com.reucon.openfire.plugin.archive.xep.AbstractIQHandler;
import com.reucon.openfire.plugin.archive.xep0059.XmppResultSet;




import org.dom4j.Element;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.JID;
import org.xmpp.packet.PacketError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Message Archiving Retrieve Handler.
 */
public class IQRetrieveHandler extends AbstractIQHandler {

    private static final Logger Log = LoggerFactory.getLogger( IQRetrieveHandler.class );
    private static final String NAMESPACE = "urn:xmpp:archive";
    int random = new Random().nextInt(1000);
    
    public IQRetrieveHandler() {
        super("Message Archiving Retrieve Handler", "retrieve", NAMESPACE);
    }
    
	private String getUserName(String username) {
		if(username==null) return "";
		if(username.contains("@")) {
	         	 int index = username.indexOf("@");
	             username = username.substring(0, index);
	     }
		return username;
		
	}
	
	private boolean canAccessFast(long frompoint) {
		boolean icanAccessMSet=false;
		 	MemoryPoint point=new MemoryPoint();
		    Map<Long, String> points = point.getPoint();
		    
	   
	    if (points!=null)
	    for (Map.Entry<Long, String> entry : points.entrySet())
	    {
	        long storePoint=entry.getKey();
	        String currentPointValue=entry.getValue();
	        
	        
	        
	        if(frompoint>storePoint) {
	        	if(currentPointValue!=null)
	        	if(!currentPointValue.equals("-1")) {
	        		icanAccessMSet=true;
	        		    Log.debug("IQRetrieveHandler"+random+": inPoint startDate="+new Date(frompoint)+" mapDate="+new Date(storePoint));
	        		
	        	}
	        }else{
	        			Log.debug("IQRetrieveHandler"+random+": outOfPoint startDate="+new Date(frompoint)+" mapDate="+new Date(storePoint));
	        	
	        }
	        
	    }
	    return icanAccessMSet;
	}
	private String makeChatId(String from, String to) {
		String rezult=null;
    	try{
    	String pfrom=getUserName(from);
    	String pto=getUserName(to);
    	String subkeya="";
    	String subkeyb="";
    	
    	if(pfrom.compareTo(pto)>0) {
    		// 1 - pfrom больше чем pto
    		subkeya=pfrom;
    		subkeyb=pto;
    	}else {
    		subkeya=pto;
    		subkeyb=pfrom;
    	}
    	
    	rezult=subkeya+subkeyb;
    	}catch(Exception e) {
    	 Log.error( "IQRetrieveHandler"+random+": makeChatId error.", e.toString() );
    	}
    	return rezult;
	}
	
    public IQ handleIQ(IQ packet) throws UnauthorizedException {
        final IQ reply = IQ.createResultIQ(packet);
        final RetrieveRequest retrieveRequest = new RetrieveRequest(
                packet.getChildElement());
        int fromIndex; // inclusive
        int toIndex; // exclusive
        int max;
        
        try {
        	 final Element chatElement = reply.setChildElement("chat", NAMESPACE);
        	 boolean useFastMemory=false;
         	 boolean syntaticChatId=false;
             ChatStore store=new ChatStore();
             String chatid=null;
             String lastStr=null;
             int last=-1;
             
             boolean usequinceM=false;
         	 boolean useoneH=false;
         	 //boolean existQuinceM=false;
          	 //boolean existOneH=false;
         	 //String listStartTime=null;
         	
             Date start =null;
      	     Date end =null;
      	     long frompoint=0L;
      
      	   
      	
      	    try {start =retrieveRequest.getStart(); frompoint=start.getTime();} catch (Exception ec) {}
   	        try {end   =retrieveRequest.getEnd();} catch (Exception ec) {}
   	 	    try {chatid=retrieveRequest.getChatid();} catch (Exception ec) {}
   	 	    try {lastStr=retrieveRequest.getLast(); if(lastStr!=null) {String digits = lastStr.replaceAll("[^0-9]", ""); last=Integer.parseInt(digits);}} catch (Exception ec) {}
   	 	 
   	 	
   	 	    
      	    Long quinceM = store.getTimeStore().get("quinceM");
      	   
      	   
      	    Log.error("IQRetrieveHandler"+random+": frompoint="+new Date(frompoint)+" quinceM="+new Date(quinceM)+" chatid="+chatid);
      	  
      	    Long upTime = store.getTimeStore().get("uptime");
      	    if(upTime==null) { upTime = new Date().getTime();}
      		  
      	    if(frompoint>quinceM) {
      	    	if(quinceM>upTime)
      	    	usequinceM=true;  Log.error("IQRetrieveHandler"+random+":quinceM iii1");
      	    	
      	    }
      	    if(!usequinceM){ 
      	    	Long oneH = store.getTimeStore().get("oneH");
      	    	if(frompoint>oneH) {
      	    		if(oneH>upTime)
      	    		useoneH=true;  Log.error("IQRetrieveHandler"+random+":oneH iii3");
      	    		
      	    	}
      	    }
      	    List<ArchivedMessage> messagesList=new ArrayList<ArchivedMessage>();
      	    if(usequinceM) {
      	    	Log.error("IQRetrieveHandler"+random+": usequinceM="+usequinceM+" chatid="+chatid);
      	    	Map<String, ConcurrentHashMap<String, ArchivedMessage>> conversationsquinceM = store.getMessageStorequinceM();
     	    	if(conversationsquinceM!=null){
     	    		
    	 			if(conversationsquinceM.containsKey(chatid)) {
    	 				ConcurrentHashMap<String, ArchivedMessage> value = conversationsquinceM.get(chatid);
    	 				 chatElement.addAttribute("with", retrieveRequest.getWith());
    	 		         chatElement.addAttribute("start", XmppDateUtil.formatDate(start));
    	 		         chatElement.addAttribute("chatid", chatid);
    	 		         chatElement.addAttribute("hot", "15m");
//High load iterator     
    	 				Log.error("IQRetrieveHandler"+random+": usequinceM="+usequinceM+" size="+value.size());
        		    	for (Map.Entry<String, ArchivedMessage> entry : value.entrySet())
        		    	{
        		    		String stanza=entry.getKey(); 
        		    		ArchivedMessage message=entry.getValue();
        		    		      
        		    		if(message!=null) {
        		    			Date msgdate = message.getTime();
        		    			long msgtime = msgdate.getTime();
        		    			Log.error("IQRetrieveHandler"+random+": startDate="+new Date(frompoint)+" msgtime="+new Date(msgtime));
        		    			 
        		    			if(msgtime>frompoint) {
        		    			      
        		    				if(store.getReadStorequinceM().containsKey(stanza)) {
        		    					message.setReceived("1");
        		    				}
        		    				if(store.getViewStorequinceM().containsKey(stanza)) {
        		    					message.setViewed("1");
        		    				}
        		    				
        		    				messagesList.add(message);
        		    				
        		    				  
        		    			}
        		    			
        		    		}
        		    	}
    	 			}
     	    		}
      	    }
      	    else if(useoneH) {
      	    	
      	    	Map<String, ConcurrentHashMap<String, ArchivedMessage>> conversationsoneH = store.getMessageStoreoneH();
     	    	if(conversationsoneH!=null){
     	    		Log.error("IQRetrieveHandler"+random+": useoneH="+useoneH+" chatid="+chatid);
    	 			if(conversationsoneH.containsKey(chatid)) {
    	 				 ConcurrentHashMap<String, ArchivedMessage> value = conversationsoneH.get(chatid);
    	 				 chatElement.addAttribute("with", retrieveRequest.getWith());
    	 		         chatElement.addAttribute("start", XmppDateUtil.formatDate(start));
    	 		         chatElement.addAttribute("chatid", chatid);
    	 		         chatElement.addAttribute("hot", "1h");
//High load iterator      
    	 				Log.error("IQRetrieveHandler"+random+": useoneH="+useoneH+" size="+value.size());
        		    	for (Map.Entry<String, ArchivedMessage> entry : value.entrySet())
        		    	{
        		    		String stanza=entry.getKey(); 
        		    		ArchivedMessage message=entry.getValue();
        		    		      
        		    		if(message!=null) {
        		    			Date msgdate = message.getTime();
        		    			long msgtime = msgdate.getTime();
        		    			Log.debug("IQRetrieveHandler"+random+": startDate="+new Date(frompoint)+" msgtime="+new Date(msgtime));
        		    			 
        		    			if(msgtime>frompoint) {
        		    			      
        		    				if(store.getReadStoreoneH().containsKey(stanza)) {
        		    					message.setReceived("1");
        		    				}
        		    				if(store.getViewStoreoneH().containsKey(stanza)) {
        		    					message.setViewed("1");
        		    				}
        		    				
        		    				messagesList.add(message);
        		    				
        		    				  
        		    			}
        		    			
        		    		}
        		    	}
    	 				
    	 			}
     	    		}
     	    	
      	    	
      	    }
      	  Log.error("IQRetrieveHandler"+random+": useoneH="+useoneH+" messagesList.size="+messagesList.size());  
      	  
      	  int sizeList=messagesList.size();
      	
      	  if(messagesList.size()>0) {
		    	Collections.sort(messagesList, new ArchivedMessageComparator());
		    	if(last>0) {
		    		 List<ArchivedMessage> lastmessagesList=new ArrayList<ArchivedMessage>();
		    		 if(sizeList>last) {
		    			 lastmessagesList= messagesList.subList(sizeList-last, sizeList);
		    		  }
		    		 else {
		    			 lastmessagesList= messagesList;
		    		 }
				    	for (int i = 0; i < lastmessagesList.size(); i++) {
				    		
				    		  Log.error("IQRetrieveHandler"+random+": Result stanza="+lastmessagesList.get(i).getStanza()+" body="+lastmessagesList.get(i).getBody());
				    		  if(i!=0)
				    		   addMessageElementFromMap(chatElement, lastmessagesList.get(i), lastmessagesList.get(i - 1));  
				    		  else
				    		  addMessageElementFromMap(chatElement, lastmessagesList.get(i), null);
				    	} 
		    	}else {
		    	for (int i = 0; i < messagesList.size(); i++) {
		    		
		    		  Log.error("IQRetrieveHandler"+random+": Result stanza="+messagesList.get(i).getStanza()+" body="+messagesList.get(i).getBody());
		    		  if(i!=0)
		    		   addMessageElementFromMap(chatElement, messagesList.get(i), messagesList.get(i - 1));  
		    		  else
		    		  addMessageElementFromMap(chatElement, messagesList.get(i), null);
		    	}
		    	}
		    }
      	    
      	  //messageStoreoneH - chatid
      	    
        	/*String chatid=null;
        	boolean useFastMemory=false;
        	boolean syntaticChatId=false;
        	
        	Date start =null;
     	    Date end =null;
     	    try {start = retrieveRequest.getStart();} catch (Exception ec) {}
     	    try {end = retrieveRequest.getEnd();} catch (Exception ec) {}
     	   
     		 
     	    long frompoint=1949969189;
     	    if(start!=null)
     	    frompoint=start.getTime();
     	    Log.debug("IQRetrieveHandler"+random+": startDate="+frompoint);
     	    
     	    long topoint=1949969189;
     	    if(end!=null)
     	    topoint=end.getTime();
     	    
     	   
     	    
        	useFastMemory=canAccessFast(frompoint);
        	try {chatid=retrieveRequest.getChatid();} catch (Exception ec) {}
        	JID from = packet.getFrom();
        	
        	
        	
        	if(chatid==null) {
        		chatid=makeChatId(from.toBareJID(), retrieveRequest.getWith());
        		syntaticChatId=true;
        	}else if(chatid.length()<5){
        		chatid=makeChatId(from.toBareJID(), retrieveRequest.getWith());
        		syntaticChatId=true;
        	}
        	
        	Log.debug("IQRetrieveHandler"+random+": chatid="+chatid+" useFastMemory="+useFastMemory+" syntatic="+syntaticChatId);
        	*/
        	if(useFastMemory) {/*
        	   //mem load
        		if(!chatid.isEmpty()) {
        			try {
        		if(chatid.length()>5) {
        			String  validchatid = chatid.replaceAll("[^\\w\\s\\-]","");
        	        chatElement.addAttribute("with", retrieveRequest.getWith());
        	        chatElement.addAttribute("start", XmppDateUtil.formatDate(start));
        		    chatElement.addAttribute("chatid", validchatid);

        		    if(useFastMemory) {
        		    	
        		    	List<ArchivedMessage> messagesList=new ArrayList<ArchivedMessage>();
        		    	MemorySet mset = new MemorySet();
        		    	Map<String, ConcurrentHashMap<String, ArchivedMessage>> convs = mset.getConversations();
        		    	ConcurrentHashMap<String, ArchivedMessage> conversations = new ConcurrentHashMap<String, ArchivedMessage>();
        		    	if(convs!=null) {
        		    		try {conversations = convs.get(validchatid);} catch (Exception e) {}
        		    	}
        		    	
        		    	if(conversations!=null) {
        		    	Log.debug("IQRetrieveHandler"+random+": xmessagesListsize="+conversations.size());
        		    	
        		    	for (Map.Entry<String, ArchivedMessage> entry : conversations.entrySet())
        		    	{
        		    		String stanza=entry.getKey(); 
        		    		ArchivedMessage message=entry.getValue();
        		    		      
        		    		if(message!=null) {
        		    			Date msgdate = message.getTime();
        		    			long msgtime = msgdate.getTime();
        		    			Log.debug("IQRetrieveHandler"+random+": startDate="+new Date(frompoint)+" msgtime="+new Date(msgtime));
        		    			 
        		    			if(msgtime>frompoint) {
        		    			      
        		    				if(MemoryDelivered.hasDeliveredElement(stanza)) {
        		    					message.setReceived("1");
        		    				}
        		    				if(MemoryViewed.hasViewElement(stanza)) {
        		    					message.setViewed("1");
        		    				}
        		    				
        		    				messagesList.add(message);
        		    				
        		    				  
        		    			}
        		    			
        		    		}
        		    	}
        		    	if(messagesList.size()>0) {
        		    	Collections.sort(messagesList, new ArchivedMessageComparator());
        		    	
        		    	for (int i = 0; i < messagesList.size(); i++) {
        		    		
        		    		  Log.debug("IQRetrieveHandler"+random+": Result stanza="+messagesList.get(i).getStanza()+" body="+messagesList.get(i).getBody());
        		    		  if(i!=0)
        		    		   addMessageElementFromMap(chatElement, messagesList.get(i), messagesList.get(i - 1));  
        		    		  else
        		    		  addMessageElementFromMap(chatElement, messagesList.get(i), null);
        		    	}}}
        		    }
        		   
        		   
        		}
        		 }catch(Exception ex) {
                	 Log.error( "Found error in Map retrive messages "+ ex.toString() ); ex.printStackTrace();
                }
        		}
        	*/
        	}
        	if(!useFastMemory&&!syntaticChatId) {
    			//db load
        		/*
		    	Log.debug("IQRetrieveHandler"+random+": get from PersistenceManager");
    			String  validchatid = chatid.replaceAll("[^\\w\\s\\-]","");
    	        chatElement.addAttribute("with", retrieveRequest.getWith());
    	        chatElement.addAttribute("start", XmppDateUtil.formatDate(start));
    		    chatElement.addAttribute("chatid", validchatid);
    		    
    		    final Conversation conversation =  getPersistenceManager(from).getConversation(from.toBareJID(), retrieveRequest.getWith(), start, end, validchatid);
    		    final List<ArchivedMessage> messages = conversation.getMessages();
        		    for (int i = 0; i < messages.size(); i++) {
        		    	    Log.debug("IQRetrieveHandler: PersistenceManager stanza="+messages.get(i).getStanza()+" body="+messages.get(i).getBody());
        	                addMessageElement(chatElement, conversation, messages.get(i), null);
        	        }
        		   
        		 */
    		}
        	 if(!usequinceM&&!useoneH){
        		//old version 
        		
        		Log.debug("IQRetrieveHandler"+random+": xget from old version");
                
        		//ignore for chatid        
        		        final Conversation conversation = retrieve(packet.getFrom(), retrieveRequest);

        		        if (conversation == null) {
        		            Log.debug("IQRetrieveHandler"+random+":Unable to find conversation." );
        		            return error(packet, PacketError.Condition.item_not_found);
        		        }
        		        
        		      //with='juliet@capulet.com/chamber'
        		      //start='1469-07-21T02:56:15Z'

        		       
        		        chatElement.addAttribute("with", conversation.getWithJid());
        		        chatElement.addAttribute("start", XmppDateUtil.formatDate(conversation.getStart()));
        		        chatElement.addAttribute("chatid", chatid);
        		        chatElement.addAttribute("hot", "old");
        		//max msg
        		        max = conversation.getMessages().size();
        		        fromIndex = 0;
        		        toIndex = max > 0 ? max : 0;
        		        Log.debug( "IQRetrieveHandler"+random+":Found conversation with {} messages.", max );
        		//Example 43. Requesting the first page of a collection - check if max limit is set in user request <max>100</max>
        		//ignore for chatid
        		        final XmppResultSet resultSet = retrieveRequest.getResultSet();
        		        if (resultSet != null) {
        		            if (resultSet.getMax() != null && resultSet.getMax() <= max) {
        		                max = resultSet.getMax();
        		                toIndex = fromIndex + max;
        		            }

        		            if (resultSet.getIndex() != null) {
        		                fromIndex = resultSet.getIndex();
        		                toIndex = fromIndex + max;
        		            } else if (resultSet.getAfter() != null) {
        		                fromIndex = resultSet.getAfter().intValue() + 1;
        		                toIndex = fromIndex + max;
        		            } else if (resultSet.getBefore() != null) {
        		                if (resultSet.getBefore()!=Long.MAX_VALUE)
        		                                toIndex = resultSet.getBefore().intValue();
        		                        else
        		                                toIndex = conversation.getMessages().size();
        		                fromIndex = toIndex - max;
        		            }
        		        }
        		 // max is not set
        		        
        		        
        		        
        		        fromIndex = fromIndex < 0 ? 0 : fromIndex;
        		        toIndex = toIndex > conversation.getMessages().size() ? conversation.getMessages().size() : toIndex;
        		        toIndex = toIndex < fromIndex ? fromIndex : toIndex;
        		        
        		        // <set xmlns='http://jabber.org/protocol/rsm'>
        		           //    <first index='0'>0</first>
        		          //     <last>99</last>
        		         //      <count>217</count>
        		        // </set>
        		        
        		        
        		        
        		        //String uuid = UUID.randomUUID().toString();
        		        final List<ArchivedMessage> messages = conversation.getMessages().subList(fromIndex, toIndex);
        		        //int d=messages.size();
        		        Map<String,String> vehicles = new HashMap<String,String>();

        		        for (int i = 0; i < messages.size(); i++) {

        		        	 String mstanza=""; 
        		        	 try { mstanza= messages.get(i).getStanza();}catch(Exception e) {} 
        		        	 if(!vehicles.containsKey(mstanza)){
        		        		 if (i == 0) {
        		                addMessageElement(chatElement, conversation, messages.get(i), null);
        		               
        		              
        		             
        		        		 } else {
        		                addMessageElement(chatElement, conversation, messages.get(i), messages.get(i - 1));
        		        		 }
        		        		 vehicles.put(mstanza, mstanza); 
        		        	 }
        		        }

        		        if (resultSet != null) {
        		            if (messages.size() > 0) {
        		                        resultSet.setFirst((long) fromIndex);
        		                        resultSet.setFirstIndex(fromIndex);
        		                        resultSet.setLast((long) toIndex - 1);
        		                    }
        		                    resultSet.setCount(conversation.getMessages().size());
        		                    chatElement.add(resultSet.createResultElement());
        		        }
        		        
        	}
        	
        	
        	/*
        	if(chatid!=null) {
        		//int uuid = ThreadLocalRandom.current().nextInt();

        		if(!chatid.isEmpty()) {
        			try {
        		if(chatid.length()>5) {
        			Log.debug("IQRetrieveHandler: chatid="+chatid);
        			String  validchatid = chatid.replaceAll("[^\\w\\s\\-]","");
        		    final Element chatElement = reply.setChildElement("chat", NAMESPACE);
        		    
        	        //chatElement.addAttribute("with", conversation.getWithJid());
        	        //chatElement.addAttribute("start", XmppDateUtil.formatDate(conversation.getStart()));
        	        
        		    chatElement.addAttribute("chatid", validchatid);
        		   
        		    JID from = packet.getFrom();
//!!        		    //validate retrieveRequest.getStart() BD overload
        		    

        		    
        		    if(useFastMemory) {
        		    	
        		    	List<ArchivedMessage> messagesList=new ArrayList<ArchivedMessage>();
        		    	MemorySet mset = new MemorySet();
        		    	Map<String, ConcurrentHashMap<String, ArchivedMessage>> convs = mset.getConversations();
        		    	ConcurrentHashMap<String, ArchivedMessage> conversations = new ConcurrentHashMap<String, ArchivedMessage>();
        		    	if(convs!=null) {
        		    		try {conversations = convs.get(validchatid);} catch (Exception e) {}
        		    	}
        		    	
        		    	if(conversations!=null) {
        		    	Log.debug("IQRetrieveHandler: messagesListsize="+conversations.size());
        		    	
        		    	for (Map.Entry<String, ArchivedMessage> entry : conversations.entrySet())
        		    	{
        		    		String stanza=entry.getKey(); 
        		    		ArchivedMessage message=entry.getValue();
        		    		      
        		    		if(message!=null) {
        		    			Date msgdate = message.getTime();
        		    			long msgtime = msgdate.getTime();
        		    			
        		    			 
        		    			if(msgtime>frompoint) {
        		    				//try {String ff=""; ff=" "+uuid+" {size="+conversations.size()+" !!!goodTime="+stanza+" body="+message.getBody()+"*} "; Files.write(Paths.get("/opt/openfire/logs/myfile.txt"), ff.getBytes(), StandardOpenOption.APPEND);} catch (Exception e) {}
                		           
        		    				if(MemoryDelivered.hasDeliveredElement(stanza)) {
        		    					message.setReceived("1");
        		    				}
        		    				if(MemoryViewed.hasViewElement(stanza)) {
        		    					message.setViewed("1");
        		    				}
        		    				
        		    				messagesList.add(message);
        		    				
        		    				  
        		    			}else {
        		    				 //try {String ff=""; ff=" "+uuid+" {size="+conversations.size()+" outOfTimeMessage stanza="+stanza+" body="+message.getBody()+" msgtime="+msgtime+"*} "; Files.write(Paths.get("/opt/openfire/logs/myfile.txt"), ff.getBytes(), StandardOpenOption.APPEND);} catch (Exception e) {}
                 		            
        		    			}
        		    			
        		    		}
        		    	}
        		    	if(messagesList.size()>0) {
        		    	Collections.sort(messagesList, new ArchivedMessageComparator());
        		    	//Conversation conversation=new Conversation (start, end, validchatid, validchatid, validchatid, validchatid, validchatid, validchatid);
        		    	for (int i = 0; i < messagesList.size(); i++) {
        		    		
        		    		  Log.debug("IQRetrieveHandler: Result stanza="+messagesList.get(i).getStanza()+" body="+messagesList.get(i).getBody());
        		    		  if(i!=0)
        		    		   addMessageElementFromMap(chatElement, messagesList.get(i), messagesList.get(i - 1));  
        		    		  else
        		    		  addMessageElementFromMap(chatElement, messagesList.get(i), null);
        		    	}}}
        		    }
        		   
        		    else{   
        		    	Log.debug("IQRetrieveHandler: get from PersistenceManager");
	        		    final Conversation conversation =  getPersistenceManager(from).getConversation(from.toBareJID(), retrieveRequest.getWith(), start, end, validchatid);
	        		    final List<ArchivedMessage> messages = conversation.getMessages();
		        		    for (int i = 0; i < messages.size(); i++) {
		        		    	    Log.debug("IQRetrieveHandler: PersistenceManager stanza="+messages.get(i).getStanza()+" body="+messages.get(i).getBody());
		        	                addMessageElement(chatElement, conversation, messages.get(i), null);
		        	        }
		        		   
        		    }
        		}
        		 }catch(Exception ex) {
                	 Log.error( "Found error in Map retrive messages "+ ex.toString() ); ex.printStackTrace();
                }
        		}
        	}else {	
       
        
//ignore for chatid        
        final Conversation conversation = retrieve(packet.getFrom(), retrieveRequest);

        if (conversation == null) {
            Log.debug( "Unable to find conversation." );
            return error(packet, PacketError.Condition.item_not_found);
        }
        
      //with='juliet@capulet.com/chamber'
      //start='1469-07-21T02:56:15Z'

        final Element chatElement = reply.setChildElement("chat", NAMESPACE);
        chatElement.addAttribute("with", conversation.getWithJid());
        chatElement.addAttribute("start", XmppDateUtil.formatDate(conversation.getStart()));
//max msg
        max = conversation.getMessages().size();
        fromIndex = 0;
        toIndex = max > 0 ? max : 0;
        Log.debug( "Found conversation with {} messages.", max );
//Example 43. Requesting the first page of a collection - check if max limit is set in user request <max>100</max>
//ignore for chatid
        final XmppResultSet resultSet = retrieveRequest.getResultSet();
        if (resultSet != null) {
            if (resultSet.getMax() != null && resultSet.getMax() <= max) {
                max = resultSet.getMax();
                toIndex = fromIndex + max;
            }

            if (resultSet.getIndex() != null) {
                fromIndex = resultSet.getIndex();
                toIndex = fromIndex + max;
            } else if (resultSet.getAfter() != null) {
                fromIndex = resultSet.getAfter().intValue() + 1;
                toIndex = fromIndex + max;
            } else if (resultSet.getBefore() != null) {
                if (resultSet.getBefore()!=Long.MAX_VALUE)
                                toIndex = resultSet.getBefore().intValue();
                        else
                                toIndex = conversation.getMessages().size();
                fromIndex = toIndex - max;
            }
        }
 // max is not set
        
        
        
        fromIndex = fromIndex < 0 ? 0 : fromIndex;
        toIndex = toIndex > conversation.getMessages().size() ? conversation.getMessages().size() : toIndex;
        toIndex = toIndex < fromIndex ? fromIndex : toIndex;
        
        // <set xmlns='http://jabber.org/protocol/rsm'>
           //    <first index='0'>0</first>
          //     <last>99</last>
         //      <count>217</count>
        // </set>
        
        
        
        //String uuid = UUID.randomUUID().toString();
        final List<ArchivedMessage> messages = conversation.getMessages().subList(fromIndex, toIndex);
        //int d=messages.size();
        Map<String,String> vehicles = new HashMap<String,String>();

        for (int i = 0; i < messages.size(); i++) {

        	 String mstanza=""; 
        	 try { mstanza= messages.get(i).getStanza();}catch(Exception e) {} 
        	 if(!vehicles.containsKey(mstanza)){
        		 if (i == 0) {
                addMessageElement(chatElement, conversation, messages.get(i), null);
               
              
             
        		 } else {
                addMessageElement(chatElement, conversation, messages.get(i), messages.get(i - 1));
        		 }
        		 vehicles.put(mstanza, mstanza); 
        	 }
        }

        if (resultSet != null) {
            if (messages.size() > 0) {
                        resultSet.setFirst((long) fromIndex);
                        resultSet.setFirstIndex(fromIndex);
                        resultSet.setLast((long) toIndex - 1);
                    }
                    resultSet.setCount(conversation.getMessages().size());
                    chatElement.add(resultSet.createResultElement());
        }
        }
        	*/
        }catch(Exception e) {
        	 Log.error( "Found error in retrive messages.", e.toString() );
        }
        return reply;
    }

    private Conversation retrieve(JID from, RetrieveRequest request) {
        return getPersistenceManager(from).getConversation(from.toBareJID(),
                request.getWith(), request.getStart());
    }
    
    private Element addMessageElementFromMap(Element parentElement, ArchivedMessage message, ArchivedMessage previousMessage) {
        final Element messageElement;
       /*
        * ArchivedMessage conversation= new ArchivedMessage(new Date(), Direction.to, null, message.getTo(), stanza, "-1", "-1", modrequestID, delrequestID, text, delrequestID, symKeyUUID, mtype, chatid);
                    	    	conversation.setFromJid(message.getFrom());                                 
                    	    	conversation.setBody(message.getBody());
       */
        
        final long secs;
        
        if (previousMessage == null) {
            //secs = (message.getTime().getTime() - conversation.getStart().getTime()) / 1000;
            secs =0;
            
        } else {
        	
            secs = (message.getTime().getTime() - previousMessage.getTime().getTime()) / 1000;
        }
       
        
        messageElement = parentElement.addElement(message.getDirection().toString());
        messageElement.addAttribute("secs", Long.toString(secs));
        messageElement.addAttribute("millisec", Long.toString(message.getTime().getTime()));
        messageElement.addAttribute("fromjid", message.getFromJid().toBareJID());
       
        
        
        String mstate=  null;
        String mtype=   null;
        String chatid=   null;
        String mdelete= null;
        String mcorrect=null;
        //flag
        String mdeleted="";
        String modified="";
        String symKeyUUID=null;		
      
        try{mstate=message.getMstate();}catch(Exception e){}
        try{mdelete=message.getMdelete();}catch(Exception e){}
        try{mtype=message.getMtype();}catch(Exception e){}
        try{chatid=message.getChatId();}catch(Exception e){}
        try{mcorrect=message.getMcorrect();}catch(Exception e){}
        try{mdeleted=message.getMdeleted();}catch(Exception e){}
        try{modified= message.getModified();}catch(Exception e){}
       
        try{symKeyUUID = message.getSymKeyUUID();}catch(Exception e){}
        //String out=" rxz="+symKeyUUID+" \r\n";
	    //     try { Files.write(Paths.get("E:\\Openfire\\logs\\myfile.txt"), out.getBytes(), StandardOpenOption.APPEND);}catch(Exception e) {}
	         
        String stanza = "0";
        try{stanza=message.getStanza();}catch(Exception e){}
        
        String received = "0";
        try{received=message.getReceived();}catch(Exception e){}
        
        String viewed = "0";
        try{viewed=message.getViewed();}catch(Exception e){}
        
        //BGV
        //if not null stanza
        try{if(stanza==null)stanza = "-1";}catch(Exception e){}
        
        messageElement.addAttribute("stanza", stanza);
        
        try{if(received==null)received = "-1";}catch(Exception e){}
        messageElement.addAttribute("received", received);
        
        try{if(viewed==null)viewed = "-1";}catch(Exception e){}
        
        messageElement.addAttribute("viewed", viewed);
        String isysytem = "system";
        
        
        try{if(chatid   !=null) messageElement.addAttribute ("chatid",    chatid);}catch(Exception e){}
        
        
        try{if(symKeyUUID   !=null) messageElement.addAttribute ("encrypted",    symKeyUUID);}catch(Exception e){}
        try{if(mtype   !=null) messageElement.addAttribute ("mtype",    mtype);}catch(Exception e){}
        try{if(mdelete !=null) { messageElement.addAttribute ("mdelete",  mdelete); messageElement.addAttribute ("mtype",   isysytem);}}catch(Exception e){}      
        try{if(mcorrect!=null) { messageElement.addAttribute ("mcorrect", mcorrect);messageElement.addAttribute ("mtype",   isysytem);}}catch(Exception e){}
        
        try{if(mdeleted.equals("1")) {  messageElement.addAttribute("mdeleted", mdeleted);  messageElement.addAttribute ("mstate",   "deleted");}}catch(Exception e){} 
        try{if(modified.equals("1"))  { messageElement.addAttribute("mmodified", modified); messageElement.addAttribute ("mstate",   "modified");}}catch(Exception e){}
        
        if (message.getWithJid() != null) {
            messageElement.addAttribute("jid", message.getWithJid().toBareJID());
        }
        messageElement.addElement("body").setText(message.getBody());

        return messageElement;
    }
    

    private Element addMessageElement(Element parentElement,
            Conversation conversation, ArchivedMessage message, ArchivedMessage previousMessage) {
        final Element messageElement;
       
        
        final long secs;
        
        if (previousMessage == null) {
            secs = (message.getTime().getTime() - conversation.getStart().getTime()) / 1000;
            
        } else {
        	
            secs = (message.getTime().getTime() - previousMessage.getTime().getTime()) / 1000;
        }
       
        
        messageElement = parentElement.addElement(message.getDirection().toString());
        messageElement.addAttribute("secs", Long.toString(secs));
        messageElement.addAttribute("milisecs", Long.toString(message.getTime().getTime()));
        
        String mstate=  null;
        String mtype=   null;
        String chatid=   null;
        String mdelete= null;
        String mcorrect=null;
        //flag
        String mdeleted="";
        String modified="";
        String symKeyUUID=null;		
      
        try{mstate=message.getMstate();}catch(Exception e){}
        try{mdelete=message.getMdelete();}catch(Exception e){}
        try{mtype=message.getMtype();}catch(Exception e){}
        try{chatid=message.getChatId();}catch(Exception e){}
        try{mcorrect=message.getMcorrect();}catch(Exception e){}
        try{mdeleted=message.getMdeleted();}catch(Exception e){}
        try{modified= message.getModified();}catch(Exception e){}
       
        try{symKeyUUID = message.getSymKeyUUID();}catch(Exception e){}
        //String out=" rxz="+symKeyUUID+" \r\n";
	    //     try { Files.write(Paths.get("E:\\Openfire\\logs\\myfile.txt"), out.getBytes(), StandardOpenOption.APPEND);}catch(Exception e) {}
	         
        String stanza = "0";
        try{stanza=message.getStanza();}catch(Exception e){}
        
        String received = "0";
        try{received=message.getReceived();}catch(Exception e){}
        
        String viewed = "0";
        try{viewed=message.getViewed();}catch(Exception e){}
        
        //BGV
        //if not null stanza
        try{if(stanza==null)stanza = "-1";}catch(Exception e){}
        
        messageElement.addAttribute("stanza", stanza);
        
        try{if(received==null)received = "-1";}catch(Exception e){}
        messageElement.addAttribute("received", received);
        
        try{if(viewed==null)viewed = "-1";}catch(Exception e){}
        
        messageElement.addAttribute("viewed", viewed);
        String isysytem = "system";
        
        
        try{if(chatid   !=null) messageElement.addAttribute ("chatid",    chatid);}catch(Exception e){}
        
        
        try{if(symKeyUUID   !=null) messageElement.addAttribute ("encrypted",    symKeyUUID);}catch(Exception e){}
        try{if(mtype   !=null) messageElement.addAttribute ("mtype",    mtype);}catch(Exception e){}
        try{if(mdelete !=null) { messageElement.addAttribute ("mdelete",  mdelete); messageElement.addAttribute ("mtype",   isysytem);}}catch(Exception e){}      
        try{if(mcorrect!=null) { messageElement.addAttribute ("mcorrect", mcorrect);messageElement.addAttribute ("mtype",   isysytem);}}catch(Exception e){}
        
        try{if(mdeleted.equals("1")) {  messageElement.addAttribute("mdeleted", mdeleted);  messageElement.addAttribute ("mstate",   "deleted");}}catch(Exception e){} 
        try{if(modified.equals("1"))  { messageElement.addAttribute("mmodified", modified); messageElement.addAttribute ("mstate",   "modified");}}catch(Exception e){}
        
        if (message.getWithJid() != null) {
            messageElement.addAttribute("jid", message.getWithJid().toBareJID());
        }
        messageElement.addElement("body").setText(message.getBody());

        return messageElement;
    }
    
    public class ArchivedMessageComparator implements Comparator<ArchivedMessage> {
    	 
        @Override
        public int compare(ArchivedMessage s1, ArchivedMessage s2) {
        	
        	 long n1 = s1.getTime().getTime();
             long n2 = s2.getTime().getTime();
             if (n1 < n2)      return -1;
             else if (n1 > n2) return  1;
             else return 0;
           
        }
    }
}
