package com.reucon.openfire.plugin.archive.xep0136;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import org.dom4j.Element;
import org.jivesoftware.openfire.auth.UnauthorizedException;
import org.jivesoftware.openfire.disco.ServerFeaturesProvider;
import org.jivesoftware.util.XMPPDateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmpp.packet.IQ;
import org.xmpp.packet.JID;

import com.reucon.openfire.plugin.archive.model.ChatStore;
import com.reucon.openfire.plugin.archive.model.Conversation;
import com.reucon.openfire.plugin.archive.model.MemoryList;
import com.reucon.openfire.plugin.archive.util.XmppDateUtil;
import com.reucon.openfire.plugin.archive.xep.AbstractIQHandler;
import com.reucon.openfire.plugin.archive.xep0059.XmppResultSet;

/**
 * Message Archiving List Handler.
 */
public class IQListHandler extends AbstractIQHandler implements
        ServerFeaturesProvider {

    private static final Logger Log = LoggerFactory.getLogger( IQListHandler.class );
    private static final String NAMESPACE = "urn:xmpp:archive";
    private static final String NAMESPACE_MANAGE = "urn:xmpp:archive:manage";

    public IQListHandler() {
        super("Message Archiving List Handler", "list", NAMESPACE);
    }
    
    private String getUserName(String username) {
		if(username==null) return "";
		if(username.contains("@")) {
	         	 int index = username.indexOf("@");
	             username = username.substring(0, index);
	     }
		return username;
		
	}
	private String makeChatId(String from, String to) {
		String rezult=null;
    	try{
    	String pfrom=getUserName(from);
    	String pto=getUserName(to);
    	String subkeya="";
    	String subkeyb="";
    	
    	if(pfrom.compareTo(pto)>0) {
    		// 1 - pfrom больше чем pto
    		subkeya=pfrom;
    		subkeyb=pto;
    	}else {
    		subkeya=pto;
    		subkeyb=pfrom;
    	}
    	
    	rezult=subkeya+subkeyb;
    	}catch(Exception e) {
    	 Log.debug( "IQListHandler: makeChatId error.", e.toString() );
    	}
    	return rezult;
	}
    public IQ handleIQ(IQ packet) throws UnauthorizedException {
        IQ reply = IQ.createResultIQ(packet);
        ListRequest listRequest = new ListRequest(packet.getChildElement());
        JID from = packet.getFrom();
        Log.debug( "IQListHandler: Processing a request to retrieve lists. Requestor: {}", from );
       
        ChatStore store=new ChatStore();
        
       
        boolean usequinceM=false;
    	boolean useoneH=false;
    	 boolean existQuinceM=false;
     	boolean existOneH=false;
    	 String listStartTime=null;
    	
        Date start =null;
 	    Date end =null;
 	    long frompoint=0L;
 	    try {start = listRequest.getStart(); frompoint=start.getTime();} catch (Exception ec) {}
 	    try {end = listRequest.getEnd();} catch (Exception ec) {}
 	    Log.debug("IQListHandler: iii="+from.getNode());
 	   
 	   
 	    Long quinceM = store.getTimeStore().get("quinceM");
 	    Long upTime = store.getTimeStore().get("uptime");
 	    if(upTime==null) { upTime = new Date().getTime();}
 	    
 	    Log.debug("IQListHandler: frompoint="+new Date(frompoint)+" quinceM="+new Date(quinceM));
 	  
 	  
 	    if(frompoint>quinceM) {
 	    	usequinceM=true;  Log.debug("IQListHandler: iii1");
 	    	Map<String, Long> timeStorequinceM =store.getTimeStorequinceM();
	 		if(timeStorequinceM.containsKey(from.getNode())) {
	 			Log.debug("IQListHandler: iii2");
	 			if(quinceM>upTime) {
	 				existQuinceM=true;
	 			    Date result = new Date(timeStorequinceM.get(from.getNode())); 
		 			listStartTime=XmppDateUtil.formatDate(result);
		 		}
	 		 }
 	    }
 	    if(!existQuinceM) { 
 	    	Long oneH = store.getTimeStore().get("oneH");
 	    	if(frompoint>oneH) {
 	    		useoneH=true;  Log.debug("IQListHandler: iii3");
 	    		Map<String, Long> timeStoreoneH =store.getTimeStoreoneH();
 		 		if(timeStoreoneH.containsKey(from.getNode())) {
 		 			Log.debug("IQListHandler: iii4");
 		 			if(oneH>upTime) {
 		 			existOneH=true; 
 		 			Date result = new Date(timeStoreoneH.get(from.getNode())); 
 		 			listStartTime=XmppDateUtil.formatDate(result);
 		 			}
 		 		 }
 	    	}
 	    }
 	    
 	    //больше одного часа разницы
 	    
        
       
        String withJidStr=null; 
        try {
        	if(listRequest.getWith()!=null) {
        	//указан ли кто-то конкретный
        	withJidStr=listRequest.getWith();
        	}
        	} catch (Exception ec) {}
        
        String chatid=null;
        try { //указан ли Chatid
        		if(listRequest.getChatid()!=null)
        			chatid=listRequest.getChatid(); 
        } catch (Exception ec) {}
        
        
        Log.debug("IQListHandler: STEP1 startDate="+new Date(frompoint)+" chatid="+chatid+" quinceM="+new Date(quinceM)+ " withJidStr="+withJidStr);
    	 
    	 /*
    	boolean useFastMemory=false;
    	boolean syntaticChatId=false;
    	String suffix =null;
    	Date start =null;
 	    Date end =null;
 	    try {start = listRequest.getStart();} catch (Exception ec) {}
 	    try {end = listRequest.getEnd();} catch (Exception ec) {}
 	    //final Element chatElement = reply.setChildElement("chat", NAMESPACE);
 		 
 	    long frompoint=1949969189;
 	    
 	    MemoryList mlist=new MemoryList();
 	    String listStartTime=null;
 	    if(start!=null) {
 	    	
 	    	try {chatid=listRequest.getChatid();} catch (Exception ec) {}

 	    	if(chatid==null) {
 	    		chatid=makeChatId(from.toBareJID(), listRequest.getWith());
 	    		syntaticChatId=true;
 	    	}else if(chatid.length()<5){
 	    		chatid=makeChatId(from.toBareJID(), listRequest.getWith());
 	    		syntaticChatId=true;
 	    	}

 	    	suffix = new SimpleDateFormat(XMPPDateTimeFormat.XMPP_DATE_FORMAT).format(start);
 	    	  Log.debug("IQListHandler: resp start="+chatid+"_"+suffix);
 	    	if(mlist.getChatTime().containsKey(chatid+"_"+suffix)) {
 	    		listStartTime=mlist.getChatTime().get(chatid+"_"+suffix);
 	      	}
 	    	frompoint=start.getTime();
 	    }
 	    Log.debug("IQListHandler: startDate="+new Date(frompoint)+" chatid="+chatid+" listStartTime="+listStartTime+ " withJidStr="+withJidStr);
 	    
 	    long topoint=1949969189;
 	    if(end!=null)
 	    topoint=end.getTime();
 	   */
 	    
        //---------------------------------------SET LIST XML---------------------------------------------------------
        Log.debug("IQListHandler: STEP2 with="+listRequest.getWith()+" listStartTime="+listStartTime+" usequinceM="+usequinceM+" useoneH="+useoneH);
        Element listElement = reply.setChildElement("list", NAMESPACE);
        
        if((useoneH==true) || (usequinceM==true)) {
        	
        	Log.debug("IQListHandler: STEP3");
        	   if(listStartTime!=null) { 	Log.debug("IQListHandler: STEP31");
                   //Iterator include (with chatid) start filters
        		   Log.debug("IQListHandler32: rezult Iterator listStartTime="+listStartTime);
        		   if(existQuinceM) {
        			   listElement.addAttribute("hot", "15m");
    				   if(chatid!=null) {
    					   Map<String, String> listquinceM = store.getListStorequinceM();
    					   if(listquinceM.containsKey(chatid)) {
    						   String listdata=listquinceM.get(chatid);
    						   Element chatElement = listElement.addElement("chat");
        		               if(withJidStr==null) chatElement.addAttribute("with", from.asBareJID().toString());
        		               else chatElement.addAttribute("with", withJidStr);        		               
        		               chatElement.addAttribute("start",  listdata);
        		               chatElement.addAttribute("chatid", chatid);
        		               Log.debug("IQListHandler: STEP33 rezult chatid="+chatid+" start="+listdata);
    					   }
    					   
    				   }else{ 
    					   
    					Map<String, ConcurrentHashMap<String, String>> jidChatIdStorequinceM = store.getChatIdJidStorequinceM();
//NB High Load        			   
        				Log.debug("IQListHandler: STEP4");
        				   if(withJidStr!=null) {
        					   ConcurrentHashMap<String, String> chatidList=jidChatIdStorequinceM.get(withJidStr);
        					   if(chatidList!=null) {
        						   for (Map.Entry<String, String> o : chatidList.entrySet()) {
            					   Element chatElement = listElement.addElement("chat");
            		               chatElement.addAttribute("with", withJidStr);        		               
            		               chatElement.addAttribute("start",  o.getValue());
            		               chatElement.addAttribute("chatid", o.getKey());
            		               Log.debug("IQListHandler: STEP42 rezult chatid="+o.getKey()+" start="+o.getValue());
            					}
        					   
        					   }
        				   }
        				   
        				   		ConcurrentHashMap<String, String> chatids=jidChatIdStorequinceM.get(from.getNode());
        				   if(chatids!=null) {
        					   for (Map.Entry<String, String> o : chatids.entrySet()) { 
	        					   Element chatElement = listElement.addElement("chat");
	        		               if(withJidStr==null) chatElement.addAttribute("with", from.asBareJID().toString());
	        		               else chatElement.addAttribute("with", withJidStr);        		               
	        		               chatElement.addAttribute("start",  o.getValue());
	        		               chatElement.addAttribute("chatid", o.getKey());
	        		               Log.debug("IQListHandler: STEP43 rezult chatid="+o.getKey()+" start="+o.getValue());
        					   }
        				   }
    				   }
        		   }
        		   
        		   if(existOneH) {
        			   listElement.addAttribute("hot", "1h");
//NB High Load        	
        				Log.debug("IQListHandler: STEP5");
        			   if(chatid!=null) {
        				   Map<String, String> listoneH = store.getListStoreoneH();
    					   if(listoneH.containsKey(chatid)) {
    						   String listdata=listoneH.get(chatid);
    						   Element chatElement = listElement.addElement("chat");
        		               if(withJidStr==null) chatElement.addAttribute("with", from.asBareJID().toString());
        		               else chatElement.addAttribute("with", withJidStr);        		               
        		               chatElement.addAttribute("start",  listdata);
        		               chatElement.addAttribute("chatid", chatid);
        		               Log.debug("IQListHandler: STEP51 rezult chatid="+chatid+" start="+listdata);
    					   }
    					   
    				   }else{ 
    					   Map<String, ConcurrentHashMap<String, String>> jidChatIdStoreoneH = store.getChatIdJidStoreoneH();
//NB High Load        			   
       			
       				   if(withJidStr!=null) {
       					   ConcurrentHashMap<String, String> chatidList=jidChatIdStoreoneH.get(withJidStr);
       					   if(chatidList!=null) {
       						   for (Map.Entry<String, String> o : chatidList.entrySet()) {
           					   Element chatElement = listElement.addElement("chat");
           		               chatElement.addAttribute("with", withJidStr);        		               
           		               chatElement.addAttribute("start",  o.getValue());
           		               chatElement.addAttribute("chatid", o.getKey());
           		               Log.debug("IQListHandler: STEP52 rezult chatid="+o.getKey()+" start="+o.getValue());
           					}
       					   
       					   }
       				   }
       				   
       				   		ConcurrentHashMap<String, String> chatids=jidChatIdStoreoneH.get(from.getNode());
       				   if(chatids!=null) {
       					   for (Map.Entry<String, String> o : chatids.entrySet()) { 
	        					   Element chatElement = listElement.addElement("chat");
	        		               if(withJidStr==null) chatElement.addAttribute("with", from.asBareJID().toString());
	        		               else chatElement.addAttribute("with", withJidStr);        		               
	        		               chatElement.addAttribute("start",  o.getValue());
	        		               chatElement.addAttribute("chatid", o.getKey());
	        		               Log.debug("IQListHandler: STEP53 rezult chatid="+o.getKey()+" start="+o.getValue());
       					   }
       				   }
   				   
        			   }
        		   }
               
        	  	}
        	  
               Log.debug("IQListHandler: exit with="+listRequest.getWith()+" chatid="+chatid);
        }
        else{
        
        
        Collection<Conversation> conversations = list(from, listRequest);
        Log.debug( "Retrieved {} conversations for requestor {}", conversations.size(), from );
        XmppResultSet resultSet = listRequest.getResultSet();
        listElement.addAttribute("hot", "old");
        for (Conversation conversation : conversations) {
            addChatElement(listElement, conversation);
        }

        if (resultSet != null) {
            listElement.add(resultSet.createResultElement());
        }
        }
        
        
        
        
        
        
        
        
        Log.debug( "Finished processing a request to retrieve lists. Requestor: {}", from );
        return reply;
    }


    private Collection<Conversation> list(JID from, ListRequest request) {
        return getPersistenceManager(from).findConversations(request.getStart(),
                request.getEnd(), from.toBareJID(), request.getWith(),
                request.getResultSet());
    }

    private Element addChatElement(Element listElement,
            Conversation conversation) {
        Element chatElement = listElement.addElement("chat");

        chatElement.addAttribute("with", conversation.getWithJid());
        chatElement.addAttribute("start",
                XmppDateUtil.formatDate(conversation.getStart()));

        return chatElement;
    }

    public Iterator<String> getFeatures() {
        ArrayList<String> features = new ArrayList<String>();
        features.add(NAMESPACE_MANAGE);
        return features.iterator();
    }

}
