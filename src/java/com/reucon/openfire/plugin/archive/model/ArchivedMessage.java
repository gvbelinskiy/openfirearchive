package com.reucon.openfire.plugin.archive.model;

import org.jivesoftware.database.JiveID;
import org.xmpp.packet.JID;

import java.util.Date;

/**
 * An archived message.
 */
@JiveID(601)
public class ArchivedMessage {
    public enum Direction {
        /**
         * A message sent by the owner.
         */
        to,

        /**
         * A message received by the owner.
         */
        from
    }

    private Long id;
    private final Date time;
    private final Direction direction;
    private final String type;
    private String subject;
    private String body;
    private Conversation conversation;
    private JID withJid;
    private JID fromJid;
    private String stanza;
  
	private String received;
	private String viewed;
	
	private String mtype;
	private String mstate;
	private String chatid; 
	private String mdeleted;
	private String mcorrect;
	private String mdelete;
	private String modified;
	private String symKeyUUID;
	
	
	
    public ArchivedMessage(Date time, Direction direction, String type, JID withJid) {
        this.time = time;
        this.direction = direction;
        this.type = type;
        this.withJid = withJid;
    }
	public ArchivedMessage(Date time, Direction direction, String type, JID withJid,String stanza) {
		this.time = time;
		this.direction = direction;
		this.type = type;
		this.withJid = withJid;
		this.stanza=stanza;
	}
	
	//BGV
	public ArchivedMessage(Date time, Direction direction, String type, JID withJid,String stanza, String received, String viewed) {
		this.time = time;
		this.direction = direction;
		this.type = type;
		this.withJid = withJid;
		this.stanza=stanza;
		this.received=received;
		this.viewed=viewed;
	}
	public ArchivedMessage(Date time, Direction direction, String type, JID withJid,String stanza, String received, String viewed, String tmmodified,String tmdeleted,String mcorrect, String mdelete, String symKeyUUID,String mtype) {
		this.time = time;
		this.direction = direction;
		this.type = type;
		this.withJid = withJid;
		this.stanza=stanza;
		this.received=received;
		this.viewed=viewed;
		
		this.modified=tmmodified;
		this.mdeleted=tmdeleted;
		this.mcorrect=mcorrect;
		this.mdelete=mdelete;
		this.symKeyUUID=symKeyUUID;
		this.mtype=mtype;
	}
	
	public ArchivedMessage(Date time, Direction direction, String type, JID withJid,String stanza, String received, String viewed, String tmmodified,String tmdeleted,String mcorrect, String mdelete, String symKeyUUID,String mtype,String chatid) {
		this.time = time;
		this.direction = direction;
		this.type = type;
		this.withJid = withJid;
		this.stanza=stanza;
		this.received=received;
		this.viewed=viewed;
		
		this.modified=tmmodified;
		this.mdeleted=tmdeleted;
		this.mcorrect=mcorrect;
		this.mdelete=mdelete;
		this.symKeyUUID=symKeyUUID;
		this.mtype=mtype;
		this.chatid=chatid;
	}
    
	
    public String getSymKeyUUID() {
		return symKeyUUID;
	}
	public void setSymKeyUUID(String symKeyUUID) {
		this.symKeyUUID = symKeyUUID;
	}
	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public Direction getDirection() {
        return direction;
    }

    public String getType() {
        return type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getStanza() {
        return stanza;
    }

    public void setStanza(String stanza) {
        this.stanza = stanza;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    /**
     * Checks if this message contains payload that should be archived.
     *
     * @return <code>true</code> if this message is empty, <code>false</code>
     *         otherwise.
     */
    public boolean isEmpty() {
        return subject == null && body == null;
    }

    public JID getWithJid() {
        return withJid;
    }
/*
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("ArchivedMessage[id=").append(id).append(",");
        sb.append("time=").append(time).append(",");
        sb.append("stanza=").append(stanza).append(",");
        sb.append("direction=").append(direction).append("]");

        return sb.toString();
    }
    */
    
    public String getReceived() {
		return received;
	}


	@Override
	public String toString() {
		return "ArchivedMessage [id=" + id + ", time=" + time + ", direction=" + direction + ", type=" + type
				+ ", subject=" + subject + ", body=" + body + ", conversation=" + conversation + ", withJid=" + withJid
				+ ", stanza=" + stanza + ", received=" + received + ", viewed=" + viewed + ", mtype=" + mtype
				+ ", mstate=" + mstate + ", chatid=" + chatid + ", mdeleted=" + mdeleted + ", mcorrect=" + mcorrect
				+ ", mdelete=" + mdelete + ", modified=" + modified + ", symKeyUUID=" + symKeyUUID + "]";
	}
	public void setReceived(String received) {
		this.received = received;
	}


	public String getViewed() {
		return viewed;
	}


	public void setViewed(String viewed) {
		this.viewed = viewed;
	}
	public String getMtype() {
		return mtype;
	}
	public void setMtype(String mtype) {
		this.mtype = mtype;
	}
	
	
	
	public String getChatId() {
		return chatid;
	}
	public void setChatId(String chatid) {
		this.chatid = chatid;
	}
	
	public String getMstate() {
		return mstate;
	}
	public void setMstate(String mstate) {
		this.mstate = mstate;
	}
	public String getMdeleted() {
		return mdeleted;
	}
	public void setMdeleted(String mdeleted) {
		this.mdeleted = mdeleted;
	}
	public String getMcorrect() {
		return mcorrect;
	}
	public void setMcorrect(String mcorrect) {
		this.mcorrect = mcorrect;
	}
	public String getMdelete() {
		return mdelete;
	}
	public void setMdelete(String mdelete) {
		this.mdelete = mdelete;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public JID getFromJid() {
		return fromJid;
	}
	public void setFromJid(JID fromJid) {
		this.fromJid = fromJid;
	}
	
	
	
}
