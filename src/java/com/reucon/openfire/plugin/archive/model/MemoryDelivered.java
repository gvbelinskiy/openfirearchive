package com.reucon.openfire.plugin.archive.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryDelivered {
	
	static Map<String, Integer> deliveryMap=new ConcurrentHashMap<String, Integer>();

	public static Map<String, Integer> getDeliveryMap() {
		return deliveryMap;
	}

	public static void setDeliveryMap(Map<String, Integer> deliveryMap) {
		MemoryDelivered.deliveryMap = deliveryMap;
	}
	
	public static void setDeliveryElement(String stanza) {
		MemoryDelivered.deliveryMap.put(stanza, 1);
	}

	public static boolean hasDeliveredElement(String stanza) {
		if(MemoryDelivered.deliveryMap.containsKey(stanza))return true;
		else return false;
	}
}
