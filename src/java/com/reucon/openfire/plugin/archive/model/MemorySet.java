package com.reucon.openfire.plugin.archive.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MemorySet {
	
	static Map<String, ConcurrentHashMap<String, ArchivedMessage>> conversations=new ConcurrentHashMap<String, ConcurrentHashMap<String, ArchivedMessage>>();
	 /**
     * Current memory set.
     *
     * @param String index of chatid. 
     * @param ConcurrentHashMap value of stanza, ArchivedMessage. 
     */
	public  Map<String, ConcurrentHashMap<String, ArchivedMessage>> getConversations() {
		return conversations;
	}

	public  void setConversations(Map<String, ConcurrentHashMap<String, ArchivedMessage>> conversations) {
		MemorySet.conversations = conversations;
	}
	
	public  void cleanConversations() {
		MemorySet.conversations.clear();
	}
	/*
	public static void addConversation(String chatid, ArchivedMessage conversation) {
		if(MemorySet.conversations.containsKey(chatid)) {
			ConcurrentHashMap<String, ArchivedMessage> value = MemorySet.conversations.get(chatid);
			String stanza=null;
			 try { stanza= conversation.getStanza();}catch(Exception e) {}
			    if(stanza!=null) {
				   value.put(stanza, conversation);
				   MemorySet.conversations.put(chatid, value);
			}
		}
		else {
			ConcurrentHashMap<String, ArchivedMessage> value=new ConcurrentHashMap<String, ArchivedMessage>();
			String stanza=null;
			try { stanza= conversation.getStanza();}catch(Exception e) {}
			if(stanza!=null) {
				value.put(stanza, conversation);
				MemorySet.conversations.put(chatid, value);
			}
			
		}
	}*/
}
