package com.reucon.openfire.plugin.archive.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

public class ChatStore {
	//static Map<String, String> chatTime=new ConcurrentHashMap<String, String>();
	//static Map<String, String> counterStore=new ConcurrentHashMap<String, String>();
	//static Map<String, String> chatIdJidStore=new ConcurrentHashMap<String, String>();
	static Map<String, Long> timeStore=new ConcurrentHashMap<String, Long>();
	//static Map<String, String> listStore=new ConcurrentHashMap<String, String>();
	//static Map<String, String> messageStore=new ConcurrentHashMap<String, String>();
	//static Map<String, String> readStore=new ConcurrentHashMap<String, String>();
	//static Map<String, String> viewStore=new ConcurrentHashMap<String, String>();
	
	//----------------------------------quinceM------------------------------------------
	
	static Map<String, String> chatTimequinceM=new ConcurrentHashMap<String, String>();
	static Map<String, String> counterStorequinceM=new ConcurrentHashMap<String, String>();
	static Map<String, ConcurrentHashMap<String, String>> chatIdJidStorequinceM=new ConcurrentHashMap<String, ConcurrentHashMap<String, String>>();
	static Map<String, Long> timeStorequinceM=new ConcurrentHashMap<String, Long>();
	static Map<String, String> listStorequinceM=new ConcurrentHashMap<String, String>();
	static Map<String, ConcurrentHashMap<String, ArchivedMessage>> messageStorequinceM=new ConcurrentHashMap<String, ConcurrentHashMap<String, ArchivedMessage>>();
	static Map<String, String> readStorequinceM=new ConcurrentHashMap<String, String>();
	static Map<String, String> viewStorequinceM=new ConcurrentHashMap<String, String>();

	//----------------------------------oneH---------------------------------------------
	
	static Map<String, String> chatTimeoneH=new ConcurrentHashMap<String, String>();
	static Map<String, LongAdder> counterStoreoneH=new ConcurrentHashMap<String, LongAdder>();
	static Map<String, ConcurrentHashMap<String, String>> chatIdJidStoreoneH=new ConcurrentHashMap<String, ConcurrentHashMap<String, String>>();
	static Map<String, Long> timeStoreoneH=new ConcurrentHashMap<String, Long>();
	static Map<String, String> listStoreoneH=new ConcurrentHashMap<String, String>();
	static Map<String, ConcurrentHashMap<String, ArchivedMessage>>  messageStoreoneH=new ConcurrentHashMap<String, ConcurrentHashMap<String, ArchivedMessage>>();
	static Map<String, String> readStoreoneH=new ConcurrentHashMap<String, String>();
	static Map<String, String> viewStoreoneH=new ConcurrentHashMap<String, String>();

	//----------------------------------tenH---------------------------------------------	
	
	static Map<String, String> chatTimetenH=new ConcurrentHashMap<String, String>();
	static Map<String, Long> counterStoretenH=new ConcurrentHashMap<String, Long>();
	static Map<String, String> chatIdJidStoretenH=new ConcurrentHashMap<String, String>();
	static Map<String, String> timeStoretenH=new ConcurrentHashMap<String, String>();
	static Map<String, String> listStoretenH=new ConcurrentHashMap<String, String>();
	static Map<String, String> messageStoretenH=new ConcurrentHashMap<String, String>();
	static Map<String, String> readStoretenH=new ConcurrentHashMap<String, String>();
	static Map<String, String> viewStoretenH=new ConcurrentHashMap<String, String>();
	
	//----------------------------------sevenD--------------------------------------------	
	
	static Map<String, String> chatTimesevenD=new ConcurrentHashMap<String, String>();
	static Map<String, Long> counterStoresevenD=new ConcurrentHashMap<String, Long>();
	static Map<String, String> chatIdJidStoresevenD=new ConcurrentHashMap<String, String>();
	static Map<String, String> timeStoresevenD=new ConcurrentHashMap<String, String>();
	static Map<String, String> listStoresevenD=new ConcurrentHashMap<String, String>();
	static Map<String, String> messageStoresevenD=new ConcurrentHashMap<String, String>();
	static Map<String, String> readStoresevenD=new ConcurrentHashMap<String, String>();
	static Map<String, String> viewStoresevenD=new ConcurrentHashMap<String, String>();
	
	public static Map<String, Long> getTimeStore() {
		return timeStore;
	}
	public static void setTimeStore(Map<String, Long> timeStore) {
		ChatStore.timeStore = timeStore;
	}
	
	
	public void uponeH(String key) {
		counterStoreoneH.computeIfAbsent(key, k -> new LongAdder()).increment();
    }

    public long getCountoneHbyKey(String key) {
        return counterStoreoneH.getOrDefault(key, new LongAdder()).sum();
    }
	
	/*public static Map<String, String> getChatTime() {
		return chatTime;
	}
	public static void setChatTime(Map<String, String> chatTime) {
		ChatStore.chatTime = chatTime;
	}
	public static Map<String, String> getCounterStore() {
		return counterStore;
	}
	public static void setCounterStore(Map<String, String> counterStore) {
		ChatStore.counterStore = counterStore;
	}
	public static Map<String, String> getChatIdJidStore() {
		return chatIdJidStore;
	}
	public static void setChatIdJidStore(Map<String, String> chatIdJidStore) {
		ChatStore.chatIdJidStore = chatIdJidStore;
	}

	public static Map<String, String> getListStore() {
		return listStore;
	}
	public static void setListStore(Map<String, String> listStore) {
		ChatStore.listStore = listStore;
	}
	public static Map<String, String> getMessageStore() {
		return messageStore;
	}
	public static void setMessageStore(Map<String, String> messageStore) {
		ChatStore.messageStore = messageStore;
	}
	public static Map<String, String> getReadStore() {
		return readStore;
	}
	public static void setReadStore(Map<String, String> readStore) {
		ChatStore.readStore = readStore;
	}
	public static Map<String, String> getViewStore() {
		return viewStore;
	}
	public static void setViewStore(Map<String, String> viewStore) {
		ChatStore.viewStore = viewStore;
	}
	*/
	public static Map<String, String> getChatTimequinceM() {
		return chatTimequinceM;
	}
	public static void setChatTimequinceM(Map<String, String> chatTimequinceM) {
		ChatStore.chatTimequinceM = chatTimequinceM;
	}
	public static Map<String, String> getCounterStorequinceM() {
		return counterStorequinceM;
	}
	public static void setCounterStorequinceM(Map<String, String> counterStorequinceM) {
		ChatStore.counterStorequinceM = counterStorequinceM;
	}
	public static Map<String, ConcurrentHashMap<String, String>> getChatIdJidStorequinceM() {
		return chatIdJidStorequinceM;
	}
	public static void setChatIdJidStorequinceM(Map<String, ConcurrentHashMap<String, String>> chatIdJidStorequinceM) {
		ChatStore.chatIdJidStorequinceM = chatIdJidStorequinceM;
	}
	public static Map<String, Long> getTimeStorequinceM() {
		return timeStorequinceM;
	}
	public static void setTimeStorequinceM(Map<String, Long> timeStorequinceM) {
		ChatStore.timeStorequinceM = timeStorequinceM;
	}
	public static Map<String, String> getListStorequinceM() {
		return listStorequinceM;
	}
	public static void setListStorequinceM(Map<String, String> listStorequinceM) {
		ChatStore.listStorequinceM = listStorequinceM;
	}
	public static Map<String, ConcurrentHashMap<String, ArchivedMessage>> getMessageStorequinceM() {
		return messageStorequinceM;
	}
	public static void setMessageStorequinceM(Map<String, ConcurrentHashMap<String, ArchivedMessage>> messageStorequinceM) {
		ChatStore.messageStorequinceM = messageStorequinceM;
	}
	public static Map<String, String> getReadStorequinceM() {
		return readStorequinceM;
	}
	public static void setReadStorequinceM(Map<String, String> readStorequinceM) {
		ChatStore.readStorequinceM = readStorequinceM;
	}
	public static Map<String, String> getViewStorequinceM() {
		return viewStorequinceM;
	}
	public static void setViewStorequinceM(Map<String, String> viewStorequinceM) {
		ChatStore.viewStorequinceM = viewStorequinceM;
	}
	public static Map<String, String> getChatTimeoneH() {
		return chatTimeoneH;
	}
	public static void setChatTimeoneH(Map<String, String> chatTimeoneH) {
		ChatStore.chatTimeoneH = chatTimeoneH;
	}
	public static Map<String, LongAdder> getCounterStoreoneH() {
		return counterStoreoneH;
	}
	public static void setCounterStoreoneH(Map<String, LongAdder> counterStoreoneH) {
		ChatStore.counterStoreoneH = counterStoreoneH;
	}
	public static Map<String, ConcurrentHashMap<String, String>>  getChatIdJidStoreoneH() {
		return chatIdJidStoreoneH;
	}
	public static void setChatIdJidStoreoneH(Map<String, ConcurrentHashMap<String, String>> chatIdJidStoreoneH) {
		ChatStore.chatIdJidStoreoneH = chatIdJidStoreoneH;
	}
	public static Map<String, Long> getTimeStoreoneH() {
		return timeStoreoneH;
	}
	public static void setTimeStoreoneH(Map<String, Long> timeStoreoneH) {
		ChatStore.timeStoreoneH = timeStoreoneH;
	}
	public static Map<String, String> getListStoreoneH() {
		return listStoreoneH;
	}
	public static void setListStoreoneH(Map<String, String> listStoreoneH) {
		ChatStore.listStoreoneH = listStoreoneH;
	}
	public static Map<String, ConcurrentHashMap<String, ArchivedMessage>> getMessageStoreoneH() {
		return messageStoreoneH;
	}
	public static void setMessageStoreoneH(Map<String, ConcurrentHashMap<String, ArchivedMessage>> messageStoreoneH) {
		ChatStore.messageStoreoneH = messageStoreoneH;
	}
	public static Map<String, String> getReadStoreoneH() {
		return readStoreoneH;
	}
	public static void setReadStoreoneH(Map<String, String> readStoreoneH) {
		ChatStore.readStoreoneH = readStoreoneH;
	}
	public static Map<String, String> getViewStoreoneH() {
		return viewStoreoneH;
	}
	public static void setViewStoreoneH(Map<String, String> viewStoreoneH) {
		ChatStore.viewStoreoneH = viewStoreoneH;
	}
	public static Map<String, String> getChatTimetenH() {
		return chatTimetenH;
	}
	public static void setChatTimetenH(Map<String, String> chatTimetenH) {
		ChatStore.chatTimetenH = chatTimetenH;
	}
	public static Map<String, Long> getCounterStoretenH() {
		return counterStoretenH;
	}
	public static void setCounterStoretenH(Map<String, Long> counterStoretenH) {
		ChatStore.counterStoretenH = counterStoretenH;
	}
	public static Map<String, String> getChatIdJidStoretenH() {
		return chatIdJidStoretenH;
	}
	public static void setChatIdJidStoretenH(Map<String, String> chatIdJidStoretenH) {
		ChatStore.chatIdJidStoretenH = chatIdJidStoretenH;
	}
	public static Map<String, String> getTimeStoretenH() {
		return timeStoretenH;
	}
	public static void setTimeStoretenH(Map<String, String> timeStoretenH) {
		ChatStore.timeStoretenH = timeStoretenH;
	}
	public static Map<String, String> getListStoretenH() {
		return listStoretenH;
	}
	public static void setListStoretenH(Map<String, String> listStoretenH) {
		ChatStore.listStoretenH = listStoretenH;
	}
	public static Map<String, String> getMessageStoretenH() {
		return messageStoretenH;
	}
	public static void setMessageStoretenH(Map<String, String> messageStoretenH) {
		ChatStore.messageStoretenH = messageStoretenH;
	}
	public static Map<String, String> getReadStoretenH() {
		return readStoretenH;
	}
	public static void setReadStoretenH(Map<String, String> readStoretenH) {
		ChatStore.readStoretenH = readStoretenH;
	}
	public static Map<String, String> getViewStoretenH() {
		return viewStoretenH;
	}
	public static void setViewStoretenH(Map<String, String> viewStoretenH) {
		ChatStore.viewStoretenH = viewStoretenH;
	}
	public static Map<String, String> getChatTimesevenD() {
		return chatTimesevenD;
	}
	public static void setChatTimesevenD(Map<String, String> chatTimesevenD) {
		ChatStore.chatTimesevenD = chatTimesevenD;
	}
	public static Map<String, Long> getCounterStoresevenD() {
		return counterStoresevenD;
	}
	public static void setCounterStoresevenD(Map<String, Long> counterStoresevenD) {
		ChatStore.counterStoresevenD = counterStoresevenD;
	}
	public static Map<String, String> getChatIdJidStoresevenD() {
		return chatIdJidStoresevenD;
	}
	public static void setChatIdJidStoresevenD(Map<String, String> chatIdJidStoresevenD) {
		ChatStore.chatIdJidStoresevenD = chatIdJidStoresevenD;
	}
	public static Map<String, String> getTimeStoresevenD() {
		return timeStoresevenD;
	}
	public static void setTimeStoresevenD(Map<String, String> timeStoresevenD) {
		ChatStore.timeStoresevenD = timeStoresevenD;
	}
	public static Map<String, String> getListStoresevenD() {
		return listStoresevenD;
	}
	public static void setListStoresevenD(Map<String, String> listStoresevenD) {
		ChatStore.listStoresevenD = listStoresevenD;
	}
	public static Map<String, String> getMessageStoresevenD() {
		return messageStoresevenD;
	}
	public static void setMessageStoresevenD(Map<String, String> messageStoresevenD) {
		ChatStore.messageStoresevenD = messageStoresevenD;
	}
	public static Map<String, String> getReadStoresevenD() {
		return readStoresevenD;
	}
	public static void setReadStoresevenD(Map<String, String> readStoresevenD) {
		ChatStore.readStoresevenD = readStoresevenD;
	}
	public static Map<String, String> getViewStoresevenD() {
		return viewStoresevenD;
	}
	public static void setViewStoresevenD(Map<String, String> viewStoresevenD) {
		ChatStore.viewStoresevenD = viewStoresevenD;
	}
	
	 
	
}
