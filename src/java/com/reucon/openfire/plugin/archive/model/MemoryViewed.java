package com.reucon.openfire.plugin.archive.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryViewed {
	static Map<String, Integer> viewMap=new ConcurrentHashMap<String, Integer>();

	public static Map<String, Integer> getViewMap() {
		return viewMap;
	}

	public static void setViewMap(Map<String, Integer> viewMap) {
		MemoryViewed.viewMap = viewMap;
	}
	
	public static void setViewElement(String stanza) {
		MemoryViewed.viewMap.put(stanza, 1);
	}
	
	public static boolean hasViewElement(String stanza) {
		if(MemoryViewed.viewMap.containsKey(stanza)) return true;
		else return false;
	}
}
