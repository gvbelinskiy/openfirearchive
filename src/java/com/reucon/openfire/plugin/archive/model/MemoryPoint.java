package com.reucon.openfire.plugin.archive.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryPoint {
	
	static Map<Long, String> point=new ConcurrentHashMap<Long, String>();
	 /**
     * Determines memory set.
     *
     * @param long index (in whole set) of memories and statuses. 

     */
	public  Map<Long, String> getPoint() {
		return point;
	}

	public static void setPoint(Map<Long, String> point) {
		MemoryPoint.point = point;
	}
	
	public static void cleanPoints() {
		MemoryPoint.point.clear();
	}
	
	
}
