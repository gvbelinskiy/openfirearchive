package com.reucon.openfire.plugin.archive.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryList {
	static Map<String, String> chatTime=new ConcurrentHashMap<String, String>();

	public  Map<String, String> getChatTime() {
		return chatTime;
	}

	public  void setChatTime(Map<String, String> chatTime) {
		MemoryList.chatTime = chatTime;
	}
	
	
}
